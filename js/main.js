$(".main-card").slick({
  slidesToShow: 5,
  variableWidth: true,
  arrows: false,
  slidesToScroll: 1,
  swipeToSlide: true,
  integer: 1,
  initialSlide: 1,
  infinite: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        swipeToSlide: true,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        swipeToSlide: true
      }
    },
    {
      breakpoint: 6,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
$(".big-cards").slick({
  slidesToShow: 3,
  infinite: true,
  slidesToScroll: 1,
  swipeToSlide: true,
  dots: false,
  arrows: false,
  variableWidth: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        swipeToSlide: true,
        infinite: true
      }
    },
    {
      breakpoint: 769,
      infinite: true,
      dots: false,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  //
});
// $(".Modern-Slider").slick({
//   slidesToShow: 1,
//   infinite: true,
//   slidesToScroll: 1,
//   swipeToSlide: true,
//   pauseOnFocus: true,
//   // centerMode: true,
//   variableWidth: true,
//   adaptiveHeight: true,
//   dots: true,
//   nextArrow:
//     '<div class="slick-arrow next-arrow"><img src="img/next_arrow.svg" alt="" class=""></div>',
//   prevArrow:
//     '<div class="slick-arrow prev-arrow"><img src="img/prev_arrow.svg" alt="" class=""></div>'
// });

$('.slider').slick({
  autoplay: true,
  speed: 800,
  lazyLoad: 'progressive',
  arrows: true,
  dots: true,
  nextArrow:
    '<div class="slick-arrow next-arrow"><img src="img/next_arrow.svg" alt="" class=""></div>',
  prevArrow:
    '<div class="slick-arrow prev-arrow"><img src="img/prev_arrow.svg" alt="" class=""></div>'
}).slickAnimation();

//          NAVIGATION    
$("#account_btn").click(() => {
  $("#login").toggleClass("show");
});
$('.show').click(function () {
  $('this').removeClass('show');
});
$("#category1").click(function (){
  $('body ,html').toggleClass('toggled');
  $(".menu1").toggleClass("show");
    $('.blur').toggleClass('show', function () { 
      $('this').click(function(){
        $('this').removeClass("show");  
        $(".menu1").removeClass("show");
      });
    });  
});
$('.blur').click(function(){
  $('this').removeClass('show');
})
$("#category2").click(function(){
  
  $(".menu2").toggleClass("show");
    $('.blur').toggleClass('show', function () { 
      $('this').click(function(){
        $('this').removeClass("show");  
        $(".menu2").removeClass("show");
      });
    });  
    $('body ,html').toggleClass('toggled');
});
$(".blur").click(() => {
  $("#menu").toggleClass("show");
});
/// search
window.onscroll = function showHeader() {
  var header = this.document.querySelector(".navbar-fixed");

  if (window.pageYOffset > 100) {
    header.classList.add("show__fixed");
  } else {
    header.classList.remove("show__fixed");
  }
};

