const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync=require('browser-sync').create();

function style(){
    return gulp.src('./scss/styles.scss')
    .pipe(sass())
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream())
}
function watch(){
    browserSync.init({
        server:'./'
    });

    gulp.watch('./scss/**/*.scss',style);
}
exports.style = style;
exports.watch = watch;


